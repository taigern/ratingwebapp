﻿using Microsoft.EntityFrameworkCore;

namespace RatingAPI.Models
{
    public class SkillRatingContext : DbContext
    {
        public SkillRatingContext(DbContextOptions<SkillRatingContext> options):base(options)
        {

        }

        public DbSet<Skill> Skills { get; set; }    
    }

}
