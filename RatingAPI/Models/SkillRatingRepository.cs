﻿using System.Collections.Generic;
using System.Linq;

namespace RatingAPI.Models
{
    public class SkillRatingRepository : ISkillRatingRepository
    {
        private readonly SkillRatingContext _context;

        public SkillRatingRepository(SkillRatingContext context)
        {
            _context = context;

            if (_context.Skills.Count() == 0)
                Add(new Skill { Name = "C#" });
        }

        public void Add(Skill item)
        {
            _context.Skills.Add(item);
            _context.SaveChanges();
        }

        public Skill Find(long id)
        {
             return _context.Skills.FirstOrDefault(s => s.Id == id);

        }

        public IEnumerable<Skill> GetAll()
        {
            return _context.Skills.ToList();        }

        public void Remove(long id)
        {
            var item = _context.Skills.First(i => i.Id == id);
            _context.Skills.Remove(item);
            _context.SaveChanges();
        }

        public void Update(Skill item)
        {
            _context.Skills.Update(item);
            _context.SaveChanges();

        }
    }

}
