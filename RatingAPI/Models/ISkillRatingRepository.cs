﻿using System.Collections.Generic;

namespace RatingAPI.Models
{
    public interface ISkillRatingRepository
    {
        void Add(Skill item);
        IEnumerable<Skill> GetAll();
        void Remove(long id);
        Skill Find(long id);
        void Update(Skill skill);
    }

}
