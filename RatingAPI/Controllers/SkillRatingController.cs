﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RatingAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RatingAPI.Controllers
{
    [Route("api/[controller]")]
    public class SkillRatingController : Controller
    {

        private readonly ISkillRatingRepository _repository;

        public SkillRatingController(ISkillRatingRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}", Name ="GetSkill")]
        public IActionResult GetById(long id)
        {
            var item = _repository.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpGet]
        public IEnumerable<Skill> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpPost]
        public IActionResult Create([FromBody] Skill item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            _repository.Add(item);

            return CreatedAtRoute("GetSkill", new { id = item.Id, }, item);

        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Skill item)
        {
            if (item == null || item.Id != id)
            {
                return BadRequest();
            }

            var skill = _repository.Find(id);
            if (skill == null)
            {
                return NotFound();
            }

            skill.IsRated = item.IsRated;
            skill.Name = item.Name;
            skill.Rating = item.Rating;

            _repository.Update(skill);
            return new NoContentResult();
        }
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var item = _repository.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            _repository.Remove(id);
            return new NoContentResult();
        }

    }
}
